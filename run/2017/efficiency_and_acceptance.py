from pathlib import Path
from typing import Dict

from binnings import BINNINGS
from src.analysis import Analysis
from src.cutting import Cut
from utils.ROOT_utils import bayes_divide
from utils.helper_functions import smart_join
from utils.plotting_tools import Hist2dOpts
from utils.variable_names import variable_data

DTA_PATH = Path("/mnt/D/data/DTA_outputs/2024-09-19/")
# DTA_PATH = Path("/eos/home-k/kghorban/DTA_OUT/2024-02-05/")

datasets: Dict[str, Dict] = {
    # SIGNAL
    # ====================================================================
    "wtaunu": {
        "data_path": {
            "lm_cut": DTA_PATH / "*Sh_2211_Wtaunu_*_maxHTpTV2*/*.root",
            "full": DTA_PATH / "*Sh_2211_Wtaunu_mW_120*/*.root",
        },
        "hard_cut": {"lm_cut": "TruthBosonM < 120"},
        "label": r"$W\rightarrow\tau\nu$",
        "is_signal": True,
    },
}

# CUTS & SELECTIONS
# ========================================================================
pass_reco = Cut(
    r"Pass preselection",
    r"(passReco == 1) && (TauBaselineWP == 1) && (abs(TauCharge) == 1) && passMetTrigger && (badJet == 0)"
    r"&& ((MatchedTruthParticle_isTau + MatchedTruthParticle_isElectron + MatchedTruthParticle_isMuon + MatchedTruthParticle_isPhoton) <= 1)"
    r"&& ((TauNCoreTracks == 1) || (TauNCoreTracks == 3))",
)
pass_trigger = Cut(
    r"Pass trigger",
    r"passTrigger",
)
pass_met_trigger = Cut(
    r"Pass met trigger",
    r"passMetTrigger",
)
pass_truth = Cut(
    r"Pass Truth",
    r"(passTruth == 1)",
)
fail_truth = Cut(
    r"Pass Truth",
    r"!(passTruth == 1)",
)
truth_tau = Cut(
    r"Truth Hadronic Tau",
    r"TruthTau_isHadronic && ((TruthTau_nChargedTracks == 1) || (TruthTau_nChargedTracks == 3))",
)
fail_truth_tau = Cut(
    r"Truth Hadronic Tau",
    r"!TruthTau_isHadronic | ((TruthTau_nChargedTracks == 1) || (TruthTau_nChargedTracks == 3))",
)
truth_tau_1prong = Cut(
    r"1-prong truth",
    r"TruthTau_nChargedTracks == 1",
)
fail_truth_tau_1prong = Cut(
    r"Fail 1-prong truth",
    r"!(TruthTau_nChargedTracks == 1)",
)
truth_tau_3prong = Cut(
    r"3-prong truth",
    r"TruthTau_nChargedTracks == 3",
)
fail_truth_tau_3prong = Cut(
    r"Fail 3-prong truth",
    r"!(TruthTau_nChargedTracks == 3)",
)
truth_tau_plus = Cut(
    r"tau plus truth",
    r"TruthTauCharge == 1",
)
fail_truth_tau_plus = Cut(
    r"fail tau plus truth",
    r"!(TruthTauCharge == 1)",
)
truth_tau_minus = Cut(
    r"tau minus truth",
    r"TruthTauCharge == -1",
)
fail_truth_tau_minus = Cut(
    r"fail tau plus truth",
    r"!(TruthTauCharge == -1)",
)
reco_tau_1prong = Cut(
    r"1-prong Reconstructed Hadronic Tau",
    "(TauNCoreTracks == 1) && (TruthTau_nChargedTracks == 1)",
)
reco_tau_3prong = Cut(
    r"3-prong Reconstructed Hadronic Tau",
    "(TauNCoreTracks == 3) && (TruthTau_nChargedTracks == 3)",
)
reco_tau_plus = Cut(
    r"Positive Tau",
    "(TauCharge == 1) && (TruthTauCharge == 1)",
)
reco_tau_minus = Cut(
    r"Negative Tau",
    "(TauCharge == -1) && (TruthTauCharge == -1)",
)
pass_loose = Cut(
    r"\mathrm{Pass Loose ID}",
    r"(TauBDTEleScore > 0.05) && "
    r"((TauRNNJetScore > 0.15) * (TauNCoreTracks == 1) + (TauRNNJetScore > 0.25) * (TauNCoreTracks == 3))",
)
pass_medium = Cut(
    r"\mathrm{Pass Medium ID}",
    r"(TauBDTEleScore > 0.1) && "
    r"((TauRNNJetScore > 0.25) * (TauNCoreTracks == 1) + (TauRNNJetScore > 0.4) * (TauNCoreTracks == 3))",
)
pass_tight = Cut(
    r"\mathrm{Pass Tight ID}",
    r"(TauBDTEleScore > 0.15) && "
    r"((TauRNNJetScore > 0.4) * (TauNCoreTracks == 1) + (TauRNNJetScore > 0.55) * (TauNCoreTracks == 3))",
)
pass_SR_reco = Cut(
    r"Pass SR Reco",
    r"(TauPt > 170) && (MET_met > 170) && (MTW > 350)"
    r"&& ((TauNCoreTracks == 1) || (TauNCoreTracks == 3))"
    r"&& (((abs(TauEta) < 1.37) || (1.52 < abs(TauEta))) && (abs(TauEta) < 2.47))",
)
pass_SR_truth = Cut(
    r"Pass SR Truth",
    r"(VisTruthTauPt > 170) && (TruthMTW > 350) && (TruthNeutrinoPt > 170)"
    r"&& ((TruthTau_nChargedTracks == 1) || (TruthTau_nChargedTracks == 3))"
    r"&& (((abs(VisTruthTauEta) < 1.37) || (1.52 < abs(VisTruthTauEta))) && (abs(VisTruthTauEta) < 2.47))",
)
fail_SR_truth = Cut(
    r"Fail SR Truth",
    r"!(passTruth == 1)"
    r"| !(TruthTau_isHadronic) "
    r"| !((TruthTau_nChargedTracks == 1) || (TruthTau_nChargedTracks == 3)) "
    r"| !(VisTruthTauPt > 170) | !(TruthMTW > 350) | !(TruthNeutrinoPt > 170) "
    r"| !((TruthTau_nChargedTracks == 1) || (TruthTau_nChargedTracks == 3)) "
    r"| !(((abs(VisTruthTauEta) < 1.37) || (1.52 < abs(VisTruthTauEta))) && (abs(VisTruthTauEta) < 2.47))",
)
truth_cuts = [
    pass_truth,
    pass_SR_truth,
    truth_tau,
]
fail_truth_cuts = [
    # fail_truth,
    fail_SR_truth,
    # fail_truth_tau,
]
reco_cuts = [
    pass_reco,
    pass_SR_reco,
]
selections: dict[str, list[Cut]] = {
    # PASS TRUTH
    # ================================================
    "truth_tau": truth_cuts,
    "1prong_truth_tau": truth_cuts + [truth_tau_1prong],
    "3prong_truth_tau": truth_cuts + [truth_tau_3prong],
    "tauplus_truth_tau": truth_cuts + [truth_tau_plus],
    "tauminus_truth_tau": truth_cuts + [truth_tau_minus],
    # PASS RECO
    # ================================================
    "loose_reco_tau": reco_cuts + [pass_loose],
    "loose_1prong_reco_tau": reco_cuts + [pass_loose, reco_tau_1prong],
    "loose_3prong_reco_tau": reco_cuts + [pass_loose, reco_tau_3prong],
    "loose_tauplus_reco_tau": reco_cuts + [pass_loose, reco_tau_plus],
    "loose_tauminus_reco_tau": reco_cuts + [pass_loose, reco_tau_minus],
    "medium_reco_tau": reco_cuts + [pass_medium],
    "medium_1prong_reco_tau": reco_cuts + [pass_medium, reco_tau_1prong],
    "medium_3prong_reco_tau": reco_cuts + [pass_medium, reco_tau_3prong],
    "medium_tauplus_reco_tau": reco_cuts + [pass_medium, reco_tau_plus],
    "medium_tauminus_reco_tau": reco_cuts + [pass_medium, reco_tau_minus],
    "tight_reco_tau": reco_cuts + [pass_tight],
    "tight_1prong_reco_tau": reco_cuts + [pass_tight, reco_tau_1prong],
    "tight_3prong_reco_tau": reco_cuts + [pass_tight, reco_tau_3prong],
    "tight_tauplus_reco_tau": reco_cuts + [pass_tight, reco_tau_plus],
    "tight_tauminus_reco_tau": reco_cuts + [pass_tight, reco_tau_minus],
    # PASS TRUTH AND RECO
    # ================================================
    # fmt: off
    "loose_truth_reco_tau": truth_cuts + reco_cuts + [pass_loose],
    "loose_1prong_truth_reco_tau": truth_cuts + reco_cuts + [pass_loose, truth_tau_1prong,
                                                             reco_tau_1prong],
    "loose_3prong_truth_reco_tau": truth_cuts + reco_cuts + [pass_loose, truth_tau_3prong,
                                                             reco_tau_3prong],
    "loose_tauplus_truth_reco_tau": truth_cuts + reco_cuts + [pass_loose, truth_tau_plus,
                                                              reco_tau_plus],
    "loose_tauminus_truth_reco_tau": truth_cuts + reco_cuts + [pass_loose, truth_tau_minus,
                                                               reco_tau_minus],
    "medium_truth_reco_tau": truth_cuts + reco_cuts + [pass_medium],
    "medium_1prong_truth_reco_tau": truth_cuts + reco_cuts + [pass_medium, truth_tau_1prong,
                                                              reco_tau_1prong],
    "medium_3prong_truth_reco_tau": truth_cuts + reco_cuts + [pass_medium, truth_tau_3prong,
                                                              reco_tau_3prong],
    "medium_tauplus_truth_reco_tau": truth_cuts + reco_cuts + [pass_medium, truth_tau_plus,
                                                               reco_tau_plus],
    "medium_tauminus_truth_reco_tau": truth_cuts + reco_cuts + [pass_medium, truth_tau_minus,
                                                                reco_tau_minus],
    "tight_truth_reco_tau": truth_cuts + reco_cuts + [pass_tight],
    "tight_1prong_truth_reco_tau": truth_cuts + reco_cuts + [pass_tight, truth_tau_1prong,
                                                             reco_tau_1prong],
    "tight_3prong_truth_reco_tau": truth_cuts + reco_cuts + [pass_tight, truth_tau_3prong,
                                                             reco_tau_3prong],
    "tight_tauplus_truth_reco_tau": truth_cuts + reco_cuts + [pass_tight, truth_tau_plus,
                                                              reco_tau_plus],
    "tight_tauminus_truth_reco_tau": truth_cuts + reco_cuts + [pass_tight, truth_tau_minus,
                                                               reco_tau_minus],
    # fmt: on
    # PASS RECO BUT NOT TRUTH
    "loose_reco_notruth_tau": fail_truth_cuts + reco_cuts + [pass_loose],
    "loose_1prong_reco_notruth_tau": fail_truth_cuts
    + reco_cuts
    + [pass_loose, fail_truth_tau_1prong, reco_tau_1prong],
    "loose_3prong_reco_notruth_tau": fail_truth_cuts
    + reco_cuts
    + [pass_loose, fail_truth_tau_3prong, reco_tau_3prong],
    "loose_tauplus_reco_notruth_tau": fail_truth_cuts
    + reco_cuts
    + [pass_loose, fail_truth_tau_plus, reco_tau_plus],
    "loose_tauminus_reco_notruth_tau": fail_truth_cuts
    + reco_cuts
    + [pass_loose, fail_truth_tau_minus, reco_tau_minus],
    "medium_reco_notruth_tau": fail_truth_cuts + reco_cuts + [pass_medium],
    "medium_1prong_reco_notruth_tau": fail_truth_cuts
    + reco_cuts
    + [pass_medium, fail_truth_tau_1prong, reco_tau_1prong],
    "medium_3prong_reco_notruth_tau": fail_truth_cuts
    + reco_cuts
    + [pass_medium, fail_truth_tau_3prong, reco_tau_3prong],
    "medium_tauplus_reco_notruth_tau": fail_truth_cuts
    + reco_cuts
    + [pass_medium, fail_truth_tau_plus, reco_tau_plus],
    "medium_tauminus_reco_notruth_tau": fail_truth_cuts
    + reco_cuts
    + [pass_medium, fail_truth_tau_minus, reco_tau_minus],
    "tight_reco_notruth_tau": fail_truth_cuts + reco_cuts + [pass_tight],
    "tight_1prong_reco_notruth_tau": fail_truth_cuts
    + reco_cuts
    + [pass_tight, fail_truth_tau_1prong, reco_tau_1prong],
    "tight_3prong_reco_notruth_tau": fail_truth_cuts
    + reco_cuts
    + [pass_tight, fail_truth_tau_3prong, reco_tau_3prong],
    "tight_tauplus_reco_notruth_tau": fail_truth_cuts
    + reco_cuts
    + [pass_tight, fail_truth_tau_plus, reco_tau_plus],
    "tight_tauminus_reco_notruth_tau": fail_truth_cuts
    + reco_cuts
    + [pass_tight, fail_truth_tau_minus, reco_tau_minus],
}

# VARIABLES
# ========================================================================
measurement_vars_mass = [
    "TauPt",
    "VisTruthTauPt",
    "MTW",
    "TruthMTW",
    "MET_met",
    "TruthNeutrinoPt",
]
measurement_vars_unitless = [
    "TauEta",
    "VisTruthTauEta",
    "TauPhi",
    "VisTruthTauPhi",
    "TruthNeutrinoPhi",
    "MET_phi",
    "AbsDeltaPhi_tau_met",
    "TruthAbsDeltaPhi_tau_met",
    "TauPt_div_MET",
    "TruthTauPt_div_MET",
]
measurement_vars = measurement_vars_unitless + measurement_vars_mass
reco_measurement_vars = [
    v
    for v in measurement_vars
    if (variable_data[v]["tag"] == "reco") and (v not in ("TauPt_res_frac", "TauPt_res"))
]

# define 2d histograms
hists_2d = {
    "TauPt_VisTruthTauPt": Hist2dOpts("TauPt", "VisTruthTauPt", "reco_weight"),
    "TauEta_VisTruthTauEta": Hist2dOpts("TauEta", "VisTruthTauEta", "reco_weight"),
    "TauPhi_VisTruthTauPhi": Hist2dOpts("TauPhi", "VisTruthTauPhi", "reco_weight"),
    "MTW_TruthMTW": Hist2dOpts("MTW", "TruthMTW", "reco_weight"),
    "MET_met_TruthNeutrinoPt": Hist2dOpts("MET_met", "TruthNeutrinoPt", "reco_weight"),
    "MET_phi_TruthNeutrinoPhi": Hist2dOpts("MET_phi", "TruthNeutrinoPhi", "reco_weight"),
    "AbsDeltaPhi_tau_met_TruthAbsDeltaPhi_tau_met": Hist2dOpts(
        "AbsDeltaPhi_tau_met", "TruthAbsDeltaPhi_tau_met", "reco_weight"
    ),
    "TauPt_div_MET_TruthTauPt_div_MET": Hist2dOpts(
        "TauPt_div_MET", "TruthTauPt_div_MET", "reco_weight"
    ),
}
NOMINAL_NAME = "T_s1thv_NOMINAL"


def run_analysis() -> Analysis:
    """Run analysis"""

    return Analysis(
        datasets,
        year=2017,
        rerun=True,
        regen_histograms=True,
        do_systematics=False,
        # regen_metadata=True,
        ttree=NOMINAL_NAME,
        selections=selections,
        analysis_label=Path(__file__).stem,
        log_level=10,
        log_out="both",
        extract_vars=measurement_vars,
        import_missing_columns_as_nan=True,
        snapshot=False,
        hists_2d=hists_2d,
        do_unweighted=True,
        binnings={
            "": BINNINGS,
        },
    )


if __name__ == "__main__":
    # RUN
    # ========================================================================
    analysis = run_analysis()
    analysis.full_cutflow_printout(datasets=["wtaunu"])
    base_plotting_dir = analysis.paths.plot_dir

    truths = {
        "MTW": "TruthMTW",
        "TauPt": "VisTruthTauPt",
        "TauEta": "VisTruthTauEta",
        "TauPhi": "VisTruthTauPhi",
        "MET_met": "TruthNeutrinoPt",
        "MET_phi": "TruthNeutrinoPhi",
        "AbsDeltaPhi_tau_met": "TruthAbsDeltaPhi_tau_met",
        "TauPt_div_MET": "TruthTauPt_div_MET",
    }

    # print histograms
    for dataset in analysis:
        dataset.histogram_printout(to_file="txt", to_dir=analysis.paths.latex_dir)

    working_points = ("loose", "medium", "tight")
    tau_sections = ("", "1prong_", "3prong_", "tauplus_", "tauminus_")

    # CALCULATE EFFICIENCY AND ACCEPTANCE
    # ========================================================================
    for wp in working_points:
        for sec in tau_sections:
            for var in reco_measurement_vars:
                analysis.histograms[f"{wp}_{sec}{var}_efficiency"] = bayes_divide(
                    analysis.get_hist(
                        var + "_unweighted",
                        "wtaunu",
                        NOMINAL_NAME,
                        f"{wp}_{sec}truth_reco_tau",
                    ),
                    analysis.get_hist(
                        var + "_unweighted",
                        "wtaunu",
                        NOMINAL_NAME,
                        f"{sec}truth_tau",
                    ),
                )
                analysis.histograms[f"{wp}_{sec}{var}_acceptance"] = bayes_divide(
                    analysis.get_hist(
                        var + "_unweighted",
                        "wtaunu",
                        NOMINAL_NAME,
                        f"{wp}_{sec}truth_reco_tau",
                    ),
                    analysis.get_hist(
                        var + "_unweighted",
                        "wtaunu",
                        NOMINAL_NAME,
                        f"{wp}_{sec}reco_tau",
                    ),
                )
                analysis.histograms[f"{wp}_{sec}{var}_fake_rate"] = bayes_divide(
                    analysis.get_hist(
                        var + "_unweighted",
                        "wtaunu",
                        NOMINAL_NAME,
                        f"{wp}_{sec}reco_notruth_tau",
                    ),
                    analysis.get_hist(
                        var + "_unweighted",
                        "wtaunu",
                        NOMINAL_NAME,
                        f"{wp}_{sec}reco_tau",
                    ),
                )

                # Plots alone
                # =======================================================================
                default_args = {
                    "dataset": "wtaunu",
                    "systematic": NOMINAL_NAME,
                    "selection": f"{wp}_{sec}reco_tau",
                    "do_stat": True,
                    "do_syst": False,
                    "label": None,
                    "label_params": {"llabel": "Simulation", "loc": 1},
                    "title": smart_join(
                        "2017",
                        "1-prong Taus"
                        if (sec == "1prong_")
                        else ("3-prong Taus" if (sec == "3prong_") else ""),
                        f"{analysis.global_lumi / 1000:.3g}fb$^{{-1}}$",
                        sep=" | ",
                    ),
                }

                analysis.paths.plot_dir = base_plotting_dir / wp / sec
                if var in measurement_vars_mass:
                    default_args.update(
                        {"logx": True, "xlabel": variable_data[var]["name"] + " [GeV]"}
                    )
                elif var in measurement_vars_unitless:
                    default_args.update({"logx": False, "xlabel": variable_data[var]["name"]})

                # mental health
                analysis.plot(var, **default_args, filename=f"{wp}_{sec}{var}.png")

                default_args.update({"y_axlim": (0, 1.3), "hline_at": 1})

                analysis.plot(
                    val=analysis.histograms[f"{wp}_{sec}{var}_efficiency"],
                    ylabel=r"$\epsilon_\mathrm{selection}$",
                    colour="r",
                    **default_args,
                    filename=f"{wp}_{sec}{var}_efficiency.png",
                )
                analysis.plot(
                    val=analysis.histograms[f"{wp}_{sec}{var}_acceptance"],
                    ylabel=r"$f_\mathrm{in}$",
                    colour="r",
                    **default_args,
                    filename=f"{wp}_{sec}{var}_acceptance.png",
                )
                analysis.plot(
                    val=analysis.histograms[f"{wp}_{sec}{var}_fake_rate"],
                    ylabel=r"fake fraction",
                    colour="r",
                    **default_args,
                    filename=f"{wp}_{sec}{var}_fake_rate.png",
                )
                truth_label = variable_data[truths[var]]["name"] + (
                    " [GeV]" if var in measurement_vars_mass else ""
                )
                reco_label = variable_data[var]["name"] + (
                    " [GeV]" if var in measurement_vars_mass else ""
                )
                analysis.plot_2d(
                    var,
                    truths[var],
                    dataset="wtaunu",
                    systematic=NOMINAL_NAME,
                    selection=f"{wp}_{sec}truth_reco_tau",
                    ylabel=truth_label,
                    xlabel=reco_label,
                    title=f"{reco_label.removesuffix(' [GeV]')} Migration Matrix | {analysis.global_lumi / 1000:.3g}fb$^{{-1}}$",
                    labels=True,
                    logx=True if var in measurement_vars_mass else False,
                    logy=True if var in measurement_vars_mass else False,
                    label_params={"llabel": "Simulation"},
                    filename=f"{wp}_{sec}{var}_migration.png",
                )

                # resonance
                response = analysis.get_hist(
                    f"{var}_{truths[var]}",
                    dataset="wtaunu",
                    systematic=NOMINAL_NAME,
                    selection=f"{wp}_{sec}truth_reco_tau",
                ).Clone()
                response.Scale(1 / response.GetEffectiveEntries())
                analysis.plot_2d(
                    response,
                    dataset="wtaunu",
                    systematic=NOMINAL_NAME,
                    selection=f"{wp}_{sec}truth_reco_tau",
                    ylabel=truth_label,
                    xlabel=reco_label,
                    title=f"{reco_label.removesuffix(' [GeV]')} Resonance | {analysis.global_lumi / 1000:.3g}fb$^{{-1}}$",
                    labels=True,
                    logx=True if var in measurement_vars_mass else False,
                    logy=True if var in measurement_vars_mass else False,
                    label_params={"llabel": "Simulation"},
                    filename=f"{wp}_{sec}{var}_response.png",
                )

    # START OF PRONG LOOP
    # ========================================================================
    for sec in tau_sections:
        for var in reco_measurement_vars:
            default_args.update(
                {
                    "label": ["loose", "medium", "tight"],
                    "title": smart_join(
                        "1-prong Taus"
                        if (sec == "1prong_")
                        else ("3-prong Taus" if (sec == "3prong_") else ""),
                        "2017",
                        f"{analysis.global_lumi / 1000:.3g}fb$^{{-1}}$",
                        sep=" | ",
                    ),
                }
            )

            analysis.paths.plot_dir = base_plotting_dir / sec
            if var in measurement_vars_mass:
                default_args.update({"logx": True, "xlabel": variable_data[var]["name"] + " [GeV]"})
            elif var in measurement_vars_unitless:
                default_args.update({"logx": False, "xlabel": variable_data[var]["name"]})

            analysis.plot(
                val=[
                    analysis.histograms[f"loose_{sec}{var}_efficiency"],
                    analysis.histograms[f"medium_{sec}{var}_efficiency"],
                    analysis.histograms[f"tight_{sec}{var}_efficiency"],
                ],
                ylabel=r"$\epsilon_\mathrm{selection}$",
                **default_args,
                filename=f"{sec}wp_compare_{var}_efficiency.png",
            )
            analysis.plot(
                val=[
                    analysis.histograms[f"loose_{sec}{var}_acceptance"],
                    analysis.histograms[f"medium_{sec}{var}_acceptance"],
                    analysis.histograms[f"tight_{sec}{var}_acceptance"],
                ],
                ylabel=r"$f_\mathrm{in}$",
                **default_args,
                filename=f"{sec}wp_compare_{var}_acceptance.png",
            )

    # START OF WP LOOP
    # ========================================================================
    # loop over each working point
    for wp in working_points:
        for var in reco_measurement_vars:
            default_args.update(
                {
                    "label": ["1-prong", "3-prong"],
                    "title": smart_join(
                        f"{wp} WP",
                        "2017",
                        f"{analysis.global_lumi / 1000:.3g}fb$^{{-1}}$",
                        sep=" | ",
                    ),
                }
            )

            analysis.paths.plot_dir = base_plotting_dir / wp
            if var in measurement_vars_mass:
                default_args.update({"logx": True, "xlabel": variable_data[var]["name"] + " [GeV]"})
            elif var in measurement_vars_unitless:
                default_args.update({"logx": False, "xlabel": variable_data[var]["name"]})

            analysis.plot(
                val=[
                    analysis.histograms[f"{wp}_1prong_{var}_efficiency"],
                    analysis.histograms[f"{wp}_3prong_{var}_efficiency"],
                ],
                ylabel=r"$\epsilon_\mathrm{selection}$",
                **default_args,
                filename=f"{wp}_prong_compare_{var}_efficiency.png",
            )
            analysis.plot(
                val=[
                    analysis.histograms[f"{wp}_1prong_{var}_acceptance"],
                    analysis.histograms[f"{wp}_3prong_{var}_acceptance"],
                ],
                ylabel=r"$f_\mathrm{in}$",
                **default_args,
                filename=f"{wp}_prong_compare_{var}_acceptance.png",
            )

    analysis.save_hists()
    analysis.logger.info("DONE.")
